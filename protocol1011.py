import os
import socket
import struct
import sys

def make_packet(operation, start, end, client_id, length, msg):
	w1 = operation << 13
	s = 1 if start else 0
	e = 1 if end else 0
	w1 = w1 ^ (s << 12) ^ (e << 11) ^ client_id

	size = len(msg)
	return struct.pack('HH%ds' % size, w1, length, msg)


def parse_data(stream, length):
	msg, = struct.unpack('%ds' % length, stream)
	return msg

# A header is 32 bits. The first 16 bits encode a bunch of metadata (operation,
# SOS, EOS, Client ID), while the next 16 bits contain the length of the data.
def parse_header(header):
	#print "Header length was {0}".format(len(header))
	w1, = struct.unpack('H', header[:2])
	length, = struct.unpack('H', header[2:])

	operation = w1 >> 13 & 0x07
	start = w1 >> 12 & 0x01
	end = w1 >> 11 & 0x01
	client_id = w1 & 0x07ff
	# print "operation was {0}".format(operation)
	# print "start was {0}".format(start)
	# print "end was {0}".format(end)
	# print "client_id was {0}".format(client_id)
	# print "length was {0}".format(length)
	return operation, start, end, client_id, length
