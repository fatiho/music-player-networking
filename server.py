#!/usr/bin/env python

import os
import socket
import struct
import sys
from threading import Lock, Thread
from protocol1011 import *


QUEUE_LENGTH = 10
SEND_BUFFER = 10224
SERVER_IP = '127.0.0.1'
CHUNK_SIZE = 4096	
HDR_SIZE = 4

# per-client struct
class Client:
	def __init__(self, conn, addr):
		self.lock = Lock()          # For multithreading
		self.conn = conn            # Client socket
		self.addr = addr            # Host address
		self.last_song = None       # Song playing (file descriptor)
		self.is_playing = False     # Whether or not the client is playing
		self.list_requested = False # Has a list been requested?
		self.new_song = False       # Is the being sent a new file?
		self.new_list = False       # Are we sending segments from a new list?
		self.kill = False   		# Has the client been killed?

'''
Message format:
0                   1                   2                   3
|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|7|8|9|0|1|2|3|4|5|6|7|8|9|0|1|
-----------------------------------------------------------------
|Oper.|S|E|      Client ID      |           Length              |
-----------------------------------------------------------------
|                         Message...
---------------------------------------------------------------...
'''


def client_write(client, songlist):
	songlist_data = ''
	while True:
		client.lock.acquire()
		if client.kill:
			client.lock.release()
			return
		else:
			client.lock.release()

		client.lock.acquire()
		if client.is_playing:
			
			data = client.last_song.read(CHUNK_SIZE)
			start = client.new_song
			client.new_song = False
			client.lock.release()

			length = len(data)
			if length != 0:
				end = True if length < CHUNK_SIZE else False
				packet = make_packet(4, start, end, 25, length, data)
				b = client.conn.send(packet)
				if end:
					client.lock.acquire()
					client.is_playing = False
					client.lock.release()
				
		else:
			client.lock.release()

		client.lock.acquire()
		if client.list_requested:
			print "About to craft packet for songlist"
			start = client.new_list
			if start:
				songlist_data = str(songlist)
			client.new_list = False
			client.lock.release()

			length = len(songlist_data)
			end = True if length < CHUNK_SIZE else False
			if end:
				client.lock.acquire()
				client.list_requested = False
				client.lock.release()
			packet_len = length if end else CHUNK_SIZE
			packet = make_packet(3, start, end, 25, packet_len, songlist_data[:packet_len])
			b = client.conn.send(packet)
			songlist_data = songlist_data[packet_len:]
			print "list packet sent by server"
		else:
			client.lock.release()



def client_read(client, songs):
	while True:
		header = client.conn.recv(HDR_SIZE)
		op, s, e, c_id, length = parse_header(header)
		print "Received message from client with operation {0}, start {1}, end {2}, client id {3}, length {4}".format(op, s, e, c_id, length)

		packet = client.conn.recv(length) if length > 0 else ''
		
		data = parse_data(packet, length)
		client.lock.acquire()
		if op == 0: # List request
			print "client requested a list"
			client.list_requested = True
			client.new_list = True

		elif op == 1: # Play request
			song_id = int(data)
			if song_id not in songs:
				print "Invalid song ID requested: {0}".format(song_id)
				# Set error in client to TRUE
				# Set error message to "Invalid song ID"
				# Return error: Invalid Song ID
			else:
				print "Received play command for song {0}".format(song_id)
				client.is_playing = True
				song_name = songs[song_id]
				song_fd = open(song_name, 'rb')
				print "Song file descriptor: {0}".format(song_fd)
				if client.last_song is not None:
					client.last_song.close()
				client.last_song = song_fd
				client.new_song = True
		elif op == 2: # Stop request
			client.is_playing = False
			if client.last_song is not None :
			# This resets the last song's file pointer to the start of file
				client.last_song.close()
			client.last_song = None
		elif op == 7:
			client.conn.close()
			client.kill = True
			return
		else:
			print"Unsupported operation: {0}"
			# Set error in client to TRUE
			# Set error message to "Invalid song ID"
			# Return error: Unkown command
		client.lock.release()




def get_mp3s(musicdir):
	print("Reading music files...")
	songs = {}
	songlist = []
	song_id = 0
	for filename in os.listdir(musicdir):
		if not filename.endswith(".mp3"):
			continue

		# TODO: Store song metadata for future use.  You may also want to build
		# the song list once and send to any clients that need it.

		songs[song_id] = '{0}/{1}'.format(musicdir, filename)
		songlist.append((filename, str(song_id)))
		song_id += 1


	print("Found {0} song(s)!".format(len(songs)))
	return songs, songlist

def main():
	if len(sys.argv) != 3:
		sys.exit("Usage: python server.py [port] [musicdir]")
	if not os.path.isdir(sys.argv[2]):
		sys.exit("Directory '{0}' does not exist".format(sys.argv[2]))

	port = int(sys.argv[1])
	songs, songlist = get_mp3s(sys.argv[2])
	threads = []


	# print parse_packet(make_packet(4, True,  True, 3, 6, 'j23456'.encode('ascii')))

	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((SERVER_IP, port))
	s.listen(QUEUE_LENGTH)
	while True:
		# Accept new connection (should block until new connections are received)
		conn, addr = s.accept()
		# Make client instance from socket info
		client = Client(conn, addr)
		# Set up locks for new client
		t = Thread(target=client_read, args=(client, songs))
		threads.append(t)
		t.start()
		t = Thread(target=client_write, args=(client, songlist))
		threads.append(t)
		t.start()

		# Testing code. Hardocde song 0 to play immediately
		# client.lock.acquire()
		# client.last_song = songs[0]
		# client.is_playing = True
		# client.lock.release()
	s.close()


if __name__ == "__main__":
	main()
