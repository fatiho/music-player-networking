# Streaming Music Service

In this project, we designed and built a protocol for a music streaming service in which a server with music files responds to client requests for files.

### Requirements

- Our implementation is on top of raw sockets.

- Our server is able to handle multiple clients simultaneously. They should be able join and leave at any time, and the server should continue to operate seamlessly.

- Our client is interactive and it knows how to handle at least the following commands:

  - `list`: Retrieve a list of songs that are available on the server, along with their ID numbers.
  - `play [song number]`: Begin playing the song with the specified ID number. If another song is already playing, the client should switch immediately to the new one.
  - `stop`: Stops playing the current song, if there is one playing.

- The client can stream, i.e., start playing music before the mp3 has been downloaded and terminate transmission early if the client stops playing the song.

- Similar to the above, the server should be able to handle new commands without needing to finish sending the previous file. For instance, if it receives a `list` command during playback of a giant music file, the client should be able to see the list of songs immediately.

- One of the parameters of our server is a path to a directory that contains audio files. Within this directory, we assume that any file ending in ".mp3" is an mp3 audio file.

- Although our protocol should still work if the connection is slow, the song may stutter under bad network conditions.

### Protocol Design

#### Type of messages we have, and what they mean:

A jukebox client can send three requests: list, play, and stop. A server can send list segments or song segments. When a client sends a stop request, the server doesn't need to send a response. Stopping is handled independently by both client and server. The client needs no acknowledgement from the server, and the server is expected to stop at the right place.

#### Message Formatting and what they mean:

Messages will be binary. Headers will have the following fields:
Operation (3 btis):

- 000 = list request (the client is asking for a a list of songs)
- 001 = play request (the client is asking for a specific song)
- 010 = stop request (the client is telling the server it stopped the song)
- 011 = list segment (the server is sending part of a list)
- 100 = song segment (the server is sending part of a song)
- 101 = error (either server or client is complaining about something)

EOS (end of stream) (1 bit):
- 0 = not end of stream
- 1 = end of stream

SOS (start of stream) (1 bit):
- 0 = not start of stream
- 1 = start of stream
  
Misc
* Length (32 bits): length of message in bytes.
* Data (arbitrary size): Client MUST read at most `Length` bits.

#### Determining where a message ends, and where one starts

Headers contain the length of the data. In the case of a SOS, the length of the field MUST be enough to deparse the MP3 header.

#### Storing state per-client

The server needs to know the song that's playing (song ID), the status of the current song (playing vs. stopped), and a pointer to the current file position (the last bit that hasn't been sent to the client yet).

#### Messages transition the client/server from one state to another?

State transitions depend on the operation (first three bits of header:

- 000 (list request) -> Server begins transmission of list segments, until full list is sent.
- 001 (play request) -> Server begins transmission of song segments, untill song is finished or stopped.
- 010 (stop request) -> If server is playing, stop transmission. Keep track of a file pointer to last un-sent data.
- 011 (list segment) -> Display list of songs. Continue in this function synchronoyusly (i.e. blocking) until a list segment is received with the EOS bit set.
- 100 (song segment) -> One of two options:
  If client is currently playing: continue playing (i.e. continue feeding data bits to the MadFile thingy)
  Otherwise, begin playing (start new MadFile object)
- 101 (error) -> error handling function. Clients and servers MUST include well-formatted error data in the Data section.

### Checking out our code

The provided repository contains a few pieces of code:

- `setup/` contains a few bash scripts for setting up our environment
- `client.py` and `server.py` contain the main code we developed
- `mp3-example.py` is a utility file that just plays mp3 files.
- `music` contains the example mp3 files.

We developed this on our Vagrant Virtual Machine. Do note it may not work on other platforms. Try running the following from the VM directory:

```
python mp3-example.py music/cinematrik\ -\ Revolve.mp3
```
