#!/usr/bin/env python

import ao
import mad
import readline
import socket
import struct
import sys
import threading
from time import sleep
from protocol1011 import *

HDR_SIZE = 4



class mywrapper(object):
    def __init__(self):
        self.mf = None
        self.data = ""

    # When it asks to read a specific size, give it that many bytes, and
    # update our remaining data.
    def read(self, size):
        result = self.data[:size]
        self.data = self.data[size:]
        return result


# Receive messages. 
def recv_thread_func(wrap, cond_filled, sock):
    while True:
        # First read the header, i.e. the first 4 bytes
        header = sock.recv(HDR_SIZE)
        if header is None:
            break
        # Parsing the header, we obtain:
        #    op     -- Operation. Can be 4 for a song segment or 3 for a list segment.
        #    s      -- Start of Stream: True iff this segment is the first of its stream.
        #    e      -- End of Stream: True iff this segment is the last of its stream.
        #    c_id   -- Client ID: DEPRECATED
        #    length -- The expected length of the packet
        op, s, e, c_id, length = parse_header(header)

        # Try to read `length` bytes from the socket.
        packet = sock.recv(length)

        # We might read less than the expected length.
        while len(packet) < length:
            # While this happens, we must continue appending from the stream until we
            # fill the expected size.
            packet += sock.recv(length - len(packet))

        # Parse the data to obtain a stream of bytes
        data = parse_data(packet, length)
        listSent = False
        ####### START OF CRITICAL SECTION #######

        if op == 4: # Song segment
            cond_filled.acquire()

            if s:
                # print "entered new song phase"
                wrap.data = None
                wrap.mf = mad.MadFile(wrap)
                wrap.data = data
            else :
                wrap.data += data
            #print "Received packet {0}".format(count)
            if listSent :
                
            cond_filled.notify()
            cond_filled.release()
        elif op == 3: # List segment
            
            eachString = ""
            listSent = True
            enterNum = False
            firstTime = False
            eachNumAdded = False
            eachNum = ""
            list = data.decode()
            for item in list :
                if (item == '[') :
                    continue
                elif (item == '('):
                    eachString = item
                    firstTime = True
                elif (item == ')') :
                    eachString += item
                    print eachString
                    eachString = ""
                    firstTime = False
                    #enterNum = False
                elif firstTime :
                    if (item != "'"):
                        if item != "," :
                            eachString += item
                        else :
                            eachString += ":"



            

        ####### END OF CRITICAL SECTION #######



# If there is song data stored in the wrapper object, play it!
# Otherwise, wait until there is.  
def play_thread_func(wrap, cond_filled, dev):
    wrap.mf = mad.MadFile(wrap)
    while True:
        """
        TODO
        example usage of dev and wrap (see mp3-example.py for a full example):
        """
        if cond_filled is None:
            break

        cond_filled.acquire()
        while wrap.mf is None:
            cond_filled.wait()

        buf = wrap.mf.read()
        while buf is None:
            cond_filled.wait()
            buf = wrap.mf.read()


        cond_filled.release()
        dev.play(buffer(buf), len(buf))



## request from server a song


def send_request(sock, command, song_num):
    if command == 0:
        #request list
        packet = make_packet(0, False, False, 25, 0, "")
        a = sock.send(packet)
        

    elif command == 1:
        packet = make_packet(1, False, False, 25, len(song_num), song_num)
        a = sock.send(packet)
        

    elif command == 2:
        packet = make_packet(2, False, False, 25, 0, "")
        a = sock.send(packet)
        

    elif command == 7:
        packet = make_packet(7, False, False, 25, 0, "")
        a = sock.send(packet)




def main():
    if len(sys.argv) < 3:
        print 'Usage: %s <server name/ip> <server port>' % sys.argv[0]
        sys.exit(1)

    # Create a pseudo-file wrapper, condition variable, and socket.  These will
    # be passed to the thread we're about to create.
    wrap = mywrapper()

    # Create a condition variable to synchronize the receiver and player threads.
    # In python, this implicitly creates a mutex lock too.
    # See: https://docs.python.org/2/library/threading.html#condition-objects
    cond_filled = threading.Condition()

    # Create a TCP socket and try connecting to the server.
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((sys.argv[1], int(sys.argv[2])))

    # Create a thread whose job is to receive messages from the server.
    recv_thread = threading.Thread(
        target=recv_thread_func,
        args=(wrap, cond_filled, sock)
    )
    recv_thread.daemon = True
    recv_thread.start()

    # Create a thread whose job is to play audio file data.
    dev = ao.AudioDevice('pulse')
    play_thread = threading.Thread(
        target=play_thread_func,
        args=(wrap, cond_filled, dev)
    )
    play_thread.daemon = True
    play_thread.start()


    # Enter our never-ending user I/O loop.  Because we imported the readline
    # module above, raw_input gives us nice shell-like behavior (up-arrow to
    # go backwards, etc.).
    while True:
        line = raw_input('>> ')

        if ' ' in line:
            cmd, args = line.split(' ', 1)
        else:
            cmd = line
            args = None

        
        if cmd in ['l', 'list']:
            send_request(sock, 0, None)

        if cmd in ['p', 'play']:
            if args == None:
                pass
            else:
                cond_filled.acquire()
                wrap.data = ""
                wrap.mf = None
                cond_filled.release()
                send_request(sock, 1, args)



        if cmd in ['s', 'stop']:
            cond_filled.acquire()
            wrap.data = ""
            wrap.mf = None
            cond_filled.release()
            send_request(sock, 2, None)

        if cmd in ['quit', 'q', 'exit']:
            send_request(sock, 7, 0)
            sys.exit(0)

if __name__ == '__main__':
    main()
